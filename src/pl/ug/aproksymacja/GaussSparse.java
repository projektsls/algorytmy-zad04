/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;


public class GaussSparse extends Gauss {


    public GaussSparse (double[][] matrix, int[][] indexTable, int N) {
        super(matrix, indexTable, N);
    }

    @Override
    public void resetValuesUnder(int rowIndex) {
        for (int i = rowIndex + 1; i < this.size; i++) {
            if (matrix[i][rowIndex] == 0)
                continue;
            double factor = (matrix[i][rowIndex] / matrix[rowIndex][rowIndex]);
            vectorB[i] -= (factor * vectorB[rowIndex]);
            for (int j = rowIndex; j < this.size; j++)
                matrix[i][j] -= (factor * matrix[rowIndex][j]);
        }
    }

    @Override
    public void resetValuesOver(int rowIndex) {
        for (int i = 0; i <  rowIndex; i++) {
            if (matrix[i][rowIndex] == 0)
                continue;
            double factor = (matrix[i][rowIndex] / matrix[rowIndex][rowIndex]);
            vectorB[i] -= (factor * vectorB[rowIndex]);
            for (int j = rowIndex; j < this.size; j++)
                matrix[i][j] -= (factor * matrix[rowIndex][j]);
        }
    }

}
