/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;

import java.math.BigDecimal;
import java.math.BigInteger;

public class ProbabilityMatrix {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

    private int N;
    private int size;
    private double [][]matrix;
    private int [][]indexTable;
    protected BigInteger time;

    /* W indexTable sa dwie kolumny:
     *     1. Ilość Yes dla danego wiersza z macierzy glownej
     *     2. Ilość No
     */

    public ProbabilityMatrix (int n, boolean blank){
        this.N = n;
        this.size = getNumOfProbabilities();
        this.indexTable = new int[size][2];
        fillIndexTable();
    }

    public ProbabilityMatrix (int n) {
        time = BigInteger.ZERO;
        long lEndTime;
        this.N = n;
        this.size = getNumOfProbabilities();
        this.matrix = new double[size][size];

        long lStartTime = System.nanoTime();

        fillMatrix();
        this.indexTable = new int[size][2];
        fillIndexTable();
        countProbabilities();

        lEndTime = System.nanoTime();
        time = new BigInteger(Long.toString(lEndTime - lStartTime));
    }

    public BigDecimal getTime() {
        return new BigDecimal(time).divide(new BigDecimal(Integer.toString(1000000)));
    }

    public double[][] getMatrix() {
        return matrix;
    }

    public int getSize() {
        return N;
    }

    public int getCombinationSize() {
        return size;
    }

    public int[][] getIndexTable() {
        return indexTable;
    }

    private int getNumOfProbabilities(){
        int numOfProbabilities = N + 1;
        for (int i = N; i > 0; i--){
            numOfProbabilities += i;
        }
        return numOfProbabilities;
    }

    private int getNumOfCombinations(){
        int numOfCombinations = 0;
        for (int i = N - 1; i > 0; i--){
            numOfCombinations += i;
        }
        return numOfCombinations;
    }

    private void fillIndexTable (){
        int index = 0;
        for(int amountOfY = N+1, valueY = 0 ; amountOfY > 0; amountOfY--, valueY++ ) {
            for (int valueN = 0; valueN < amountOfY; valueN++, index++) {
                indexTable[index][0] = valueY;
                indexTable[index][1] = valueN;
            }
        }
    }

    private void fillMatrix(){
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size-1; j++)
                this.matrix[i][j] = 0.0;
        }
        matrix[size-1][size-1]=1;
    }

    private int findIndex (int y, int n) {      // TODO do poprawienia
        for (int i = 0; i < size; i++) {
            if (this.indexTable[i][0] == y && this.indexTable[i][1] == n) {
                return i;
            }
        }
        System.out.println("Find Index ERROR !!!");
        return -1;
    }

    private void countProbabilities (){

        /*      N = 0
         *      Y = 1
         *      U = 2
         */

        int numOfCombinations = getNumOfCombinations();


        int[] valuesForRow = new int[size];

        int options = 0;
        for (int i = N-1; i> 0; i--)
            options += i;

        // iterujemy po kolejnych wierszach macierzy glownej
        for (int matrixIndex = 1; matrixIndex < size-1; matrixIndex++) {
            if(matrixIndex == N)
                continue;

            // wypelniamy tablice pomocnicza valuesForRow[] wartosciami 0,1,2 dla danego wiersza macierzy
            int index = 0;
            for (; index < indexTable[matrixIndex][0]; index++)
                valuesForRow[index] = 1;
            for (int amountOfY = index; index<(amountOfY + indexTable[matrixIndex][1]); index++)
                valuesForRow[index] = 0;
            for (;index < size; index++)
                valuesForRow[index] = 2;

//            System.out.println("\nPrint rowTable");
//            for (int i = 0; i < size; i++) {
//                System.out.println("Index: " +i + ", value:  "+valuesForRow[i]);
//            }

            // przechodzimy przez wszytkie opcje dla danego wiersza, zliczamy ilosc Yes i No
            int yes, no;
            int a = 0, b = 1;

            for (int option = 0; option < options; option++, b++) {
                yes=0; no=0;

                if(b == N) {
                    a++;
                    b = a + 1;
                }

                for (int i = 0; i < N; i++) {
                    if (i == b) {
                        if ((valuesForRow[a] == 1 && valuesForRow[b] == 2) || (valuesForRow[a] == 2 && valuesForRow[b] == 1))
                            yes += 2;
                        else if ((valuesForRow[a] == 0 && valuesForRow[b] == 2) || (valuesForRow[a] == 2 && valuesForRow[b] == 0))
                            no += 2;
                        else if (valuesForRow[a] == 1 && valuesForRow[b] == 1)
                            yes += 2;
                        else if (valuesForRow[a] == 0 && valuesForRow[b] == 0)
                            no += 2;
                    }
                    else if(i == a)
                        continue;
                    else if (valuesForRow[i] == 1)
                        yes++;
                    else if (valuesForRow[i] == 0)
                        no++;
                }

                // sprawdzamy ktoremu indeksowi w macierzy glownej odpowiada uzyskana ilosc yes i no w danej opcji
                this.matrix[matrixIndex][findIndex(yes,no)]++;
//                System.out.println("Find index: "+ findIndex(yes,no) + ",  Yes: "+ yes+",  No: "+no);
            }

            // dzielimy wystapienia w calym wierszu macierzy glownej przez ilosc kombinacji w poj. etapie
            for (int i = 0; i < size; i++)
                this.matrix[matrixIndex][i] /= numOfCombinations;
        }
    }

    void printIndexTable(){
        System.out.println("\nIndex table: ");
        System.out.println("Nr | Y | N");
        for (int i = 0; i < size; i++) {
            System.out.println(i+" | "+this.indexTable[i][0] +" | "+this.indexTable[i][1]);
        }
    }

    void printMatrix(){
        System.out.println("\nProbability matrix: ");
        int i,j;
        System.out.print("        ");
        for (i = 0; i < size; i++)
            System.out.format("| %-4s ", "(" + this.indexTable[i][0] + "," + this.indexTable[i][1] + ")");
        System.out.println();
        for (i = 0; i < size; i++) {
            System.out.format("%-6s ", "(" + this.indexTable[i][0] + "," + this.indexTable[i][1] + ")");
            for (j = 0; j < size; j++) {
                if(this.matrix[i][j] != 0 )
                    System.out.format("| " + ANSI_RED + "%.3g\t" + ANSI_RESET, this.matrix[i][j]);
                else
                    System.out.format("| %.3g\t", this.matrix[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

}