/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;


public class Iteratives extends Probability {

    protected double[][] bottomLeftMatrix;
    protected double[][] diagonalMatrix;
    protected double[][] upperRightMatrix;
    protected int iterations=0;

    public Iteratives(double[][] matrix, int[][] indexTable, int N) {
        super(matrix, indexTable, N);

        bottomLeftMatrix = new double[this.size][this.size];
        diagonalMatrix = new double[this.size][this.size];
        upperRightMatrix = new double[this.size][this.size];
    }

    public void solve() {

        reduceDiagonal();

        splitMatrix();

        diagonalMatrixTransposition();

    }



    private void reduceDiagonal(){

        for (int i = 0; i < size; i++){
            matrix[i][i] -= vectorB[i];
            vectorB[i]--;
        }
    }

    public void splitMatrix() {

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {

                if (i < j) {
                    upperRightMatrix[i][j] = this.matrix[i][j];
                } else if (i > j) {
                    bottomLeftMatrix[i][j] = this.matrix[i][j];
                } else {
                    diagonalMatrix[i][j] = this.matrix[i][j];
                }
            }
        }

    }

    public void diagonalMatrixTransposition() {

        for (int i = 0; i < size; i++) {
            if (diagonalMatrix[i][i] != 0)
                diagonalMatrix[i][i] = 1 / diagonalMatrix[i][i];
        }

    }



    public double[][] multiplyUpperAndDiagonal(double[][] matrix, double[][] matrix2) {
        double[][] multiplySolution = new double[size][size];
        for (int i = 0; i < size; i++)
            for (int j = i+1; j < size; j++)
                multiplySolution[i][j] = matrix[i][j] * matrix2[i][i];
        return multiplySolution;
    }

    public double[][] multiplyLeftAndDiagonal(double[][] matrix, double[][] matrix2) {
        double[][] multiplySolution = new double[size][size];
        for (int i = 0; i < size; i++)
            for (int j = 0; j < i; j++)
                multiplySolution[i][j] = matrix[i][j] * matrix2[i][i];
        return multiplySolution;
    }

    public double[][] multiplyMatrices(double[][] matrix, double[][] matrix2) {
        double[][] multiplySolution = new double[size][size];
        for (int i = 0; i < size; i++)
            for (int j = 0; j < size; j++)
                multiplySolution[i][j] = matrix[i][j] * matrix2[i][i];
        return multiplySolution;
    }

    public double[] multiplyMatrixAndVector(double[][] matrix, double[] vector) {
        double[] multiplySolution = new double[size];
        for (int i = 0; i < size; i++)
            multiplySolution[i] = vector[i] * matrix[i][i];
        return multiplySolution;
    }

    public void printMatrix(double[][] testMatrix) {
        for (double[] row : testMatrix) {
            for (int j = 0; j < size; j++) {
                System.out.print(row[j] + "\t");
            }
            System.out.println();
        }
    }

    public int getIterations(){
        return iterations;
    }

}

