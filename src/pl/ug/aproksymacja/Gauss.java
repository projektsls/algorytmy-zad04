/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2018-12-08
 *  @Purpose    przeprowadzenie pomiarów czasu działania metod Gaussa, Gaussa-Seidela i gotowej metody rozwiazywania rzadkiego.
 *              Znalezienie wielomianu aproksymacyjnego dla kazdej z serii pomiarów
 */

package pl.ug.aproksymacja;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;


public class Gauss extends Probability {

    BigDecimal solution[];

    public Gauss (double[][] matrix, int[][] indexTable, int N) {
        super(matrix, indexTable, N);
        backupValues();
    }


    public void test() {
        precision = BigDecimal.ZERO;
        time = BigInteger.ZERO;
        long lStartTime;
        long lEndTime;
        for (int i = 0; i < trials; i++) {
//            System.out.print(" " + i);
            backupValues();

            lStartTime = System.nanoTime();

            gaussHalf();

            lEndTime = System.nanoTime();
//            System.out.println("\t"+lEndTime - lStartTime);
            time = time.add(new BigInteger(Long.toString(lEndTime - lStartTime)));
        }
    }

    public void gaussHalf() {

        for (int i = 0; i<size-1; i++) {
            matrix[i][i]--;
            vectorB[i] = 0;
        }
        vectorB[size-1] = 1;

        for (int k = 0; k < this.size; k++) {
            int maxRowIndex = getPivotRow(k);
            double pivot = matrix[maxRowIndex][k];
            if (pivot == 0.0) {
                continue;
            }

            normalizePivotRow(k, maxRowIndex, pivot);
            swapRows(k, maxRowIndex);
            resetValuesUnder(k);
            resetValuesOver(k);


        }
        getSolutionValues();
    }


    public int getPivotRow(int rowIndex) {
        int maxRowIndex = rowIndex;
        for (int i = rowIndex + 1; i < this.size; i++)
            if (Math.abs(matrix[i][rowIndex]) > Math.abs(matrix[maxRowIndex][rowIndex]))
                maxRowIndex = i;
        return maxRowIndex;
    }



    public void swapRows(int rowIndex, int maxRowIndex) {
        double tmp;
        for (int i = rowIndex; i < this.size; i++) {
            tmp = (matrix[rowIndex][i]);
            matrix[rowIndex][i] = (matrix[maxRowIndex][i]);
            matrix[maxRowIndex][i] = (tmp);
        }
        tmp = (vectorB[rowIndex]);
        vectorB[rowIndex] = (vectorB[maxRowIndex]);
        vectorB[maxRowIndex] = (tmp);
    }

    public void normalizePivotRow(int rowIndex, int maxRowIndex, double pivot) {
        for (int i = rowIndex; i < this.size; i++) {
            matrix[maxRowIndex][i] = (matrix[maxRowIndex][i] / pivot);
        }
        vectorB[maxRowIndex] /= pivot;
    }

    public void resetValuesUnder(int rowIndex) {
        for (int i = rowIndex + 1; i < this.size; i++) {
            double factor = (matrix[i][rowIndex] / matrix[rowIndex][rowIndex]);
            vectorB[i] -= (factor * vectorB[rowIndex]);
            for (int j = rowIndex; j < this.size; j++)
                matrix[i][j] -= (factor * matrix[rowIndex][j]);
        }
    }

    public void resetValuesOver(int rowIndex) {
        for (int i = 0; i <  rowIndex; i++) {
            double factor = (matrix[i][rowIndex] / matrix[rowIndex][rowIndex]);
            vectorB[i] -= (factor * vectorB[rowIndex]);
            for (int j = rowIndex; j < this.size; j++)
                matrix[i][j] -= (factor * matrix[rowIndex][j]);
        }
    }

    public void getSolutionValues() {
        for (int i = this.size - 2; i > N; i--) {
            if (matrix[i][i] == 0.0 ) {
                vectorX[i] = 0;
                continue;
            }
            vectorX[i] = vectorB[i] / matrix[i][i];
        }
    }

    public void toFile() {

        String DELIMITER = ";";
        String NEW_LINE = "\n";

        File file = new File("gauss-"+N+".csv");
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);

            fileWriter.append("size");
            fileWriter.append(NEW_LINE);
            fileWriter.append(String.format("%20d", N));
            fileWriter.append(DELIMITER);

            fileWriter.append(NEW_LINE);
            fileWriter.append("combination"+DELIMITER+"gauss");
            fileWriter.append(NEW_LINE);
            fileWriter.append(DELIMITER);

            for (int i = 0; i<size; i++) {
                fileWriter.append(indexTable[i][0]+"-"+indexTable[i][1]);
                fileWriter.append(DELIMITER);
                fileWriter.append(String.format("%.20f", vectorX[i]));
                fileWriter.append(NEW_LINE);
            }

        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error");
                e.printStackTrace();
            }
            System.out.println("CSV file created");
        }
    }

}

