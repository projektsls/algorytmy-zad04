/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;

import java.math.BigDecimal;
import java.math.BigInteger;

public class GaussSeidel extends Iteratives {

    public GaussSeidel(double[][] matrix, int[][] indexTable, int N) {
        super(matrix, indexTable, N);
        this.backupValues();
    }

    public void test(double epsilon) {
        precision = BigDecimal.ZERO;
        time = BigInteger.ZERO;
        long lStartTime;
        long lEndTime;
        for (int i = 0; i < trials; i++) {
//            System.out.print(" "+i);
            backupValues();

            lStartTime = System.nanoTime();

            solve(epsilon);

            lEndTime = System.nanoTime();
            time = time.add(new BigInteger(Long.toString(lEndTime - lStartTime)));
//            System.out.println("Time: "+Long.toString(lEndTime - lStartTime));

        }
//        System.out.println("Avg Time: "+getTime());
//        System.out.println("Ilosc iteracji: " + getIterations());
//        printSolution();
    }


    public void solve(double eps) {
        super.solve();
        iterations = 0;
        double[] previousVectorX = new double[size];

        vectorB[size - 1] = 1;
        matrix[size - 1][size - 1] = 1;

        while (true) {
            iterations++;
            for(int i = 0; i < size; i++){
                double sum = vectorB[i];
                for(int j = 0; j < i; j++){
                    sum-=matrix[i][j]*vectorX[j];
                }
                for(int j = i+1; j < size; j++){
                    sum-=matrix[i][j]*previousVectorX[j];
                }
                vectorX[i]=sum/matrix[i][i];
            }


            double error = 0;

            for (int i = 0; i < size; i++) {
                error += Math.abs(vectorX[i] - previousVectorX[i]);
            }

            error /= size;

            if (error < eps) {
                break;
            }

            for (int i = 0; i < size; i++) {
                previousVectorX[i] = vectorX[i];
            }
        }
    }

}

