/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;


public class Probability {

    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

    protected double[][] originalMatrix;
    protected double[][] matrix;
    protected double[] vectorXdoZmiany;
    protected double[] vectorB;
    protected double[] vectorX;
    protected int size;
    protected BigDecimal precision ;
    protected BigInteger time;
    protected int[][] indexTable;
    protected int N;
    protected int trials = 5;       // TODO zmienic


    public Probability(double[][] matrix, int[][] indexTable, int N) {
        this.N = N;
        this.size = matrix.length;
        this.originalMatrix = matrix;
        this.matrix = new double[size][size];
        this.vectorB = new double[size];
        this.vectorX = new double[size];
        this.indexTable = indexTable;
    }


    public void setPrecision(BigDecimal precision) {
        this.precision = precision;
    }

    public BigDecimal getPrecision() {
        return this.precision;
    }

    public double[] getVectorX() {
        return vectorX;
    }


    public double getTime() {
        BigDecimal result = BigDecimal.ZERO;
        result = new BigDecimal(time).divide(new BigDecimal(Integer.toString(trials)), MathContext.DECIMAL128);
        return result.divide(new BigDecimal(Integer.toString(1000000)), MathContext.DECIMAL128).doubleValue();
    }

    public void setTime(BigInteger time) {
        this.time = time;
    }

    public void backupValues() {
        for (int i = 0; i < this.size; i++) {
            for (int j = 0; j < this.size; j++) {
                this.matrix[i][j] = this.originalMatrix[i][j];
            }
        }
        int i;
        for (i = 0; i < this.size-1; i++) {
            vectorX[i] = 0;
            this.vectorB[i] = 1;
        }
        vectorX[i] = 1.0;
        vectorB[i] = 1;
    }


    public void printRawMatrix() {
        System.out.println("Matrix");
        for (double[] row : matrix) {
            for (int j = 0; j < this.size; j++) {
                System.out.print(row[j] + "\t");
            }
            System.out.println();
        }
    }

    void printMatrix(){
        System.out.println("\nMatrix: ");
        int i,j;
        System.out.print("       ");
        for (i = 0; i < size; i++)
            System.out.format("| %-6s ", "P(" + this.indexTable[i][0] + "," + this.indexTable[i][1] + ")");
        System.out.println();
        for (i = 0; i < size; i++) {
            System.out.format("%-6s ", "P(" + this.indexTable[i][0] + "," + this.indexTable[i][1] + ")");
            for (j = 0; j < size; j++) {
                if(this.matrix[i][j] != 0 )
                    System.out.format("| " + ANSI_RED + "%.5g " + ANSI_RESET, this.matrix[i][j]);
                else
                    System.out.format("| %.5g ", this.matrix[i][j]);
            }
            System.out.println();
        }
    }

    public void printSolution() {
        System.out.println("VectorX");
        for (int i = 0; i<size; i++) {
            System.out.print("P("+indexTable[i][0]+","+indexTable[i][1]+") = ");
            System.out.format("| %.3g \n", vectorX[i]);
        }
        System.out.println();
    }

    public void printVectorB() {
        System.out.println("Vector B");
        for (double el : vectorB)
            System.out.println(el);
        System.out.println();
    }


    public void printTogether() {
//        System.out.println("Matrix | Vector B");
        for (int i = 0; i < this.size; i++) {
            System.out.print("("+i+") \t");
            for (int j = 0; j < this.size; j++) {
                if(this.matrix[i][j] == 0 )
                    System.out.format("%.2g \t", matrix[i][j]);
                else
                    System.out.format(ANSI_RED + "%.2g " + "\t" + ANSI_RESET, matrix[i][j]);
            }
            if(this.vectorB[i] == 0 )
                System.out.format(" | %.2g \t", vectorB[i]);
            else System.out.format(" | "+ANSI_RED +"%.2g \t" + ANSI_RESET, vectorB[i]);

            System.out.println();
        }
        System.out.println();
    }
}
