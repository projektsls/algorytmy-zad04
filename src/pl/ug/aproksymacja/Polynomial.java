/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;

import org.ejml.simple.SimpleMatrix;

public class Polynomial {

    private int degree;
    private double[][] gaussTimeMatrix;
    private double[][] matrix;
    private double[] vectorB;
    private double[] vectorX;
    private int factorSize;
    private int size;

    /*
        Przyjmuje macierz dwukolumnowa
            pierwsza kolumna to wielkosc testowanej macierzy (tu ilosc kombinacji dla danej liczby agentow)
            druga to czas wykonania
     */


    public Polynomial(double[][] matrix, int degree) {
        this.gaussTimeMatrix = matrix;
        this.degree = degree;
        this.size = matrix.length;

//        System.out.println("Agents \tGauss time");
//        for (int p = 0; p<size; p++) {
//            System.out.print(gaussTimeMatrix[p][0]);
//            System.out.print("\t");
//            System.out.println(gaussTimeMatrix[p][1]);
//        }

        fillMatrix();
        solveMatrix();
    }

    public void solveMatrix() {
        SimpleMatrix A = new SimpleMatrix(matrix);
        SimpleMatrix B = new SimpleMatrix(this.factorSize, 1);
        SimpleMatrix solution = new SimpleMatrix(this.factorSize, 1);
        for (int j = 0; j < this.factorSize; j++) {
            B.setRow(j, 0, vectorB[j]);
        }

        solution = A.solve(B);
//        System.out.println("After solve with library");
//        solution.print();
        for (int j = 0; j < this.factorSize; j++) {
            vectorX[j] = solution.get(j, 0);
        }
    }


    private void fillMatrix() {
        factorSize = degree + 1;
        matrix = new double[factorSize][factorSize];

        for (int i = 0; i < factorSize; i++) {
            for (int j = 0; j < factorSize; j++) {
                matrix[i][j] = getS(i + j);
            }
        }


        vectorB = new double[factorSize];
        for (int i = 0; i < this.factorSize; i++) {
            vectorB[i] = getT(i);
        }


        vectorX = new double[factorSize];
        for (int i = 0; i < this.factorSize; i++) {
            vectorX[i] = 0;
        }
    }

    private double getS(int k) {
        double result = 0;
        for(int i = 0; i<size; i++){
            result += Math.pow(gaussTimeMatrix[i][0], k);
//            System.out.println(gaussTimeMatrix[i][0]+"^"+k+" = "+Math.pow(gaussTimeMatrix[i][0], k)+" + prev = "+result);
        }
        return result;
    }

    private double getT(int k) {
        double result = 0;
        for(int i = 0; i<size; i++){
            result += (gaussTimeMatrix[i][1] * Math.pow(gaussTimeMatrix[i][0], k));
        }
        return result;
    }


    public double getEstimatedTime(int number) {
        double time = vectorX[0];
        int magnified = 1;
        for (int i = 1; i < this.factorSize; i++) {
            magnified *= number;
            time += (magnified * vectorX[i]);
        }
        return time;
    }


    public void printPolynomial() {
        System.out.println("Polynomial: \t");
        for (int i = factorSize - 1; i > 0; i--) {
            System.out.format("(%.4g)x^"+i+" + ", vectorX[i]);
        }
        System.out.println(vectorX[0]);
    }


    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";

    public void printTogether() {
        System.out.println("Matrix | Vector B");
        for (int i = 0; i < this.factorSize; i++) {
            System.out.print("(" + i + ") \t");
            for (int j = 0; j < this.factorSize; j++) {
                if (this.matrix[i][j] == 0)
                    System.out.format("%.4g \t", matrix[i][j]);
                else
                    System.out.format(ANSI_RED + "%.4g " + "\t" + ANSI_RESET, matrix[i][j]);
            }
            if (this.vectorB[i] == 0)
                System.out.format(" | %.4g \t", vectorB[i]);
            else System.out.format(" | " + ANSI_RED + "%.4g \t" + ANSI_RESET, vectorB[i]);

            System.out.println();
        }
        System.out.println();
    }


}
