/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;

public class TestPolynomial {

    private static final String DELIMITER = ";";
    private static final String NEW_LINE = "\n";


    TestPolynomial(int size) {
        String filename = "Estimated-time-test-onesize-" + String.valueOf(size) + ".csv";
        printToFile(filename, size, size, 1);
    }

    TestPolynomial(int minSize, int maxSize, int coIle) {
        String filename = "Estimated-time-tests-to-" + String.valueOf(maxSize) + "-every-" + String.valueOf(coIle) + ".csv";
        printToFile(filename, minSize, maxSize, coIle);
    }


    public void printToFile(String filename, int minSize, int maxSize, int coIle) {

        String header = "Agents;" +
                "Build-time;" +
                "Gauss-time;" +
                "GaussSparse-time;GaussSeidel-time;SparseMatrix-time";

        File file = new File(filename);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.append(header);
            fileWriter.append(NEW_LINE);


            int resultsSize = 0;
            for (int agents = minSize; agents <= maxSize; agents += coIle) {
                resultsSize++;
            }

            double[][] resultsGauss = new double[resultsSize][2];
            double[][] resultsGaussSparse = new double[resultsSize][2];
            double[][] resultsGaussSeidel = new double[resultsSize][2];
            double[][] resultsSparseMatrix = new double[resultsSize][2];
            int i = 0;

            for (int agents = minSize; agents <= maxSize; agents += coIle, i++) {

                System.out.println("\n\n[   SIZE " + agents + "  ]\n");
                fileWriter.append(String.format("%10d", agents));
                fileWriter.append(DELIMITER);


                ProbabilityMatrix p = new ProbabilityMatrix(agents);
                System.out.println("\tBuild matrix");

                fileWriter.append(String.format("%.20f", p.getTime()));
                fileWriter.append(DELIMITER);


                System.out.println("\tGauss");
                Gauss gauss = new Gauss(p.getMatrix(), p.getIndexTable(), p.getSize());
                gauss.test();
                resultsGauss[i][0] = p.getSize();
                resultsGauss[i][1] = gauss.getTime();
                fileWriter.append(String.format("%.20f", gauss.getTime()));
                fileWriter.append(DELIMITER);


                System.out.println("\tGaussSparse");
                GaussSparse gaussSparse = new GaussSparse(p.getMatrix(), p.getIndexTable(), p.getSize());
                gaussSparse.test();
                resultsGaussSparse[i][0] = p.getSize();
                resultsGaussSparse[i][1] = gaussSparse.getTime();
                fileWriter.append(String.format("%.20f", gaussSparse.getTime()));
                fileWriter.append(DELIMITER);


                System.out.println("\tGauss-Seidel:");
                GaussSeidel gaussSeidel = new GaussSeidel(p.getMatrix(), p.getIndexTable(), p.getSize());
                gaussSeidel.test(0.0000000001);
                resultsGaussSeidel[i][0] = p.getSize();
                resultsGaussSeidel[i][1] = gaussSeidel.getTime();
                fileWriter.append(String.format("%.20f", gaussSeidel.getTime()));
                fileWriter.append(DELIMITER);


                System.out.println("\tSparseMatrix");
                SparseMatrix sparseMatrix = new SparseMatrix(p.getMatrix(), p.getIndexTable(), p.getSize());
                sparseMatrix.test();
                resultsSparseMatrix[i][0] = p.getSize();
                resultsSparseMatrix[i][1] = sparseMatrix.getTime();
                fileWriter.append(String.format("%.20f", sparseMatrix.getTime()));


                fileWriter.append(NEW_LINE);

            }

            fileWriter.append(NEW_LINE);
            fileWriter.append(NEW_LINE);
            fileWriter.append(NEW_LINE);

            System.out.println("\tEstimating time\n");
            fileWriter.append("Agents;Estimated-gauss-time;Estimated-gauss-sparse-time;Estimated-gauss-seidel-time;Estimated-sparse-matrix");
            fileWriter.append(NEW_LINE);

            System.out.println("\n======= GAUSS =======\n");
            Polynomial polynomialGauss = new Polynomial(resultsGauss, 3);
            System.out.println("\n======= GAUSS Sparse =======\n");
            Polynomial polynomialGaussSparse = new Polynomial(resultsGaussSparse, 2);
            System.out.println("\n======= GAUSS Seidel =======\n");
            Polynomial polynomialGaussSeidel = new Polynomial(resultsGaussSeidel, 2);
            System.out.println("\n======= SparseMatrix =======\n");
            Polynomial polynomialSparseMatrix = new Polynomial(resultsSparseMatrix, 1);

            System.out.println("\n");

            for (int agents = minSize; agents <= maxSize; agents += coIle) {
                fileWriter.append(String.format("%10d", agents));
                fileWriter.append(DELIMITER);
                System.out.print("Size:  " + agents);

                fileWriter.append(String.format("%.20f", polynomialGauss.getEstimatedTime(agents)));
                fileWriter.append(DELIMITER);
                System.out.println("\tEstimated gauss:   " + polynomialGauss.getEstimatedTime(agents));

                fileWriter.append(String.format("%.20f", polynomialGaussSparse.getEstimatedTime(agents)));
                fileWriter.append(DELIMITER);
                System.out.println("\tEstimated gauss sparse:   " + polynomialGaussSparse.getEstimatedTime(agents));

                fileWriter.append(String.format("%.20f", polynomialGaussSeidel.getEstimatedTime(agents)));
                fileWriter.append(DELIMITER);
                System.out.println("\tEstimated gauss seidel:   " + polynomialGaussSeidel.getEstimatedTime(agents));

                fileWriter.append(String.format("%.20f", polynomialSparseMatrix.getEstimatedTime(agents)));
                fileWriter.append(DELIMITER);
                System.out.println("\tEstimated sparse matrix:   " + polynomialSparseMatrix.getEstimatedTime(agents));

                System.out.println("");


                fileWriter.append(NEW_LINE);
            }

            System.out.println("Polynomial Gauss");
            polynomialGauss.printPolynomial();
            System.out.println("Polynomial Gauss Sparse");
            polynomialGaussSparse.printPolynomial();
            System.out.println("Polynomial Gauss Seidel");
            polynomialGaussSeidel.printPolynomial();
            System.out.println("Polynomial Sparse Matrix");
            polynomialSparseMatrix.printPolynomial();
            System.out.println("\tEstimated gauss (100 000):   " + polynomialGauss.getEstimatedTime(446));
            System.out.println("\tEstimated gauss sparse (100 000):   " + polynomialGaussSparse.getEstimatedTime(446));
            System.out.println("\tEstimated gauss seidel (100 000):   " + polynomialGaussSeidel.getEstimatedTime(446));
            System.out.println("\tEstimated sparse matrix (100 000):   " + polynomialSparseMatrix.getEstimatedTime(446));

        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        } finally {
            try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error");
                e.printStackTrace();
            }
            System.out.println("\nCSV file " + filename + " created");
        }
    }
}
