/*
 *  @Authors    Sebastian Piaścik, Łukasz Pikor, Sonia Stenzel
 *  @Date       2019-01-13
 */

package pl.ug.aproksymacja;

import org.ejml.LinearSolverSparseSafe;
import org.ejml.data.*;
import org.ejml.interfaces.linsol.LinearSolverSparse;
import org.ejml.sparse.FillReducing;
import org.ejml.sparse.csc.factory.LinearSolverFactory_DSCC;

import java.math.BigDecimal;
import java.math.BigInteger;

// =====  Efficient Java Matrix Library , http://ejml.org/javadoc/org/ejml/LinearSolverSparseSafe.html ====
public class SparseMatrix extends Probability {
    DMatrixSparseCSC sparseMatrix = new DMatrixSparseCSC(size,size);
    DMatrixSparseCSC sparseVectorB = new DMatrixSparseCSC(size,1);
    DMatrixSparseCSC sparseVectorX = new DMatrixSparseCSC(size,1);
    LinearSolverSparse<DMatrixSparseCSC,DMatrixRMaj> solver = LinearSolverFactory_DSCC.lu(FillReducing.NONE);


    public SparseMatrix(double[][] matrix, int[][] indexTable, int N) {
        super(matrix, indexTable, N);
        backupValues();
        solver = new LinearSolverSparseSafe<>(solver);
    }

    public void test() {
        precision = BigDecimal.ZERO;
        time = BigInteger.ZERO;
        long lStartTime;
        long lEndTime;
        for (int i = 0; i < trials; i++) {
            System.out.print(" " + i);
            backupValues();

            lStartTime = System.nanoTime();

            solver.solveSparse(sparseVectorB,sparseVectorX);

            lEndTime = System.nanoTime();
            if(i>0)
                time = time.add(new BigInteger(Long.toString(lEndTime - lStartTime)));
        }
        System.out.println();
    }

    @Override
    public void backupValues() {
        super.backupValues();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                sparseMatrix.set(i,j,matrix[i][j]);
            }
        }
        for (int i = 0; i < size; i++) {
            sparseVectorB.set(i,0,vectorB[i]);
        }
        for (int i = 0; i < size; i++) {
            sparseVectorX.set(i,0,0);
        }
        for (int j = 0; j < size - 1; j++) {
            sparseMatrix.set(j,j,matrix[j][j]-1);
            sparseVectorB.set(j,0,0);
        }
        sparseVectorB.set(size-1,0,1);
        solver.setA(sparseMatrix);
    }
}
// ========